package ru.t1.nkiryukhin.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ApplicationVersionResponse extends AbstractResponse {

    private String version;

}
