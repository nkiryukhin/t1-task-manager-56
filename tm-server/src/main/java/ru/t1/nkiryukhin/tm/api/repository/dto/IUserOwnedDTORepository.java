package ru.t1.nkiryukhin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    void clear(@NotNull String userId);

    @Nullable
    List<M> findAll(@NotNull String userId);

    List<M> findAll(@NotNull String userId, @Nullable Comparator comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    int getSize(@NotNull String userId);

}
