package ru.t1.nkiryukhin.tm.api.repository.model;

import ru.t1.nkiryukhin.tm.model.Project;


public interface IProjectRepository extends IUserOwnedRepository<Project> {
}