package ru.t1.nkiryukhin.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nkiryukhin.tm.component.Bootstrap;
import ru.t1.nkiryukhin.tm.configuration.LoggerConfiguration;


public final class Application {

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}